#!/usr/bin/env bash


###### OUTGOING IPS TO DENY #######
### Reject traffic from host machine to outgoing ips below
export DENY_OUT_LIST=(
    10.0.0.0/8
    172.16.0.0/12
    192.168.0.0/16
    100.64.0.0/10
    198.18.0.0/15
    169.254.0.0/16
    0.0.0.0/8
)
#########################################
#
#
###### INCOME PORTS TO ALLOW #######
export INCOME_PORTS_ALLOW=(
    22      # Allow SSH into machine
)
#
#
