#!/usr/bin/env bash
#
. rules.sh

sudo ufw reset

# revert back ip route restrictions
for ip in "${DENY_OUT_LIST[@]}"
do
    sudo ip route delete unreachable $ip
    echo "Delete $ip from being unreachable"
done
