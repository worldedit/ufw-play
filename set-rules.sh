#!/usr/bin/env bash
. rules.sh

echo "--------------"

# apply reject IP's
for ip in "${DENY_OUT_LIST[@]}"
do
    sudo ufw reject out from any to $ip
    echo "Add reject OUT traffic to IP: $ip"
done

echo "--------------"

# apply ip route restrictions
for ip in "${DENY_OUT_LIST[@]}"
do
    sudo ip route add unreachable $ip
    echo "Add $ip to unreachable ip route"
done

echo "--------------"

# apply allow ports
for port in "${INCOME_PORTS_ALLOW[@]}"
do
    sudo ufw allow $port
    echo "Add allow IN traffic to port: $port"
done

echo "--------------"

# enable & reload UFW
sudo ufw enable
sudo ufw reload
echo "Ufw enabled && reloaded!"

echo "--------------"

echo "All is done 🚀"
